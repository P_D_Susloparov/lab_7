
<?php
/**
 * Реализовать возможность входа с паролем и логином с использованием
 * сессии для изменения отправленных данных в предыдущей задаче,
 * пароль и логин генерируются автоматически при первоначальной отправке формы.
 */
function generate($number)
{
    $arr = array('a','b','c','d','e','f',
        'g','h','i','j','k','l',
        'm','n','o','p','r','s',
        't','u','v','x','y','z',
        'A','B','C','D','E','F',
        'G','H','I','J','K','L',
        'M','N','O','P','R','S',
        'T','U','V','X','Y','Z',
        '1','2','3','4','5','6',
        '7','8','9','0');
// Генерируем пароль
    $pass = "";
    for($i = 0; $i < $number; $i++)
    {
// Вычисляем случайный индекс массива
        $index = rand(0, count($arr) - 1);
        $pass .= $arr[$index];
    }
    return $pass;
}
// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Массив для временного хранения сообщений пользователю.
    $messages = array();

    print '

    <head>
        <script src="https://kit.fontawesome.com/e2ac9cc532.js" crossorigin="anonymous"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link  href="style-form.css" rel="stylesheet"  media="all"/>
    </head>
            <form action="admin.php" accept-charset="UTF-8" method="GET">
                    <input  style="margin-bottom:-130px;color:white;" type="submit" id="send" class="buttonform" value="Войти как администратор">
                </div>
        </div>
        </form>
';
    // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
    // Выдаем сообщение об успешном сохранении.
    if (!empty($_COOKIE['save'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('save', '', 100000);
        setcookie('login', '', 100000);
        setcookie('pass', '', 100000);
        // Если есть параметр save, то выводим сообщение пользователю.
        $messages[] = 'Спасибо, результаты сохранены.';
        if(strip_tags($_COOKIE['admin'])=='1'){
            header('Location:login.php');
        }

        if (!empty($_COOKIE['pass'])) {
            $messages[] = sprintf('<br/> <a style="color:#e6b333;" href="login.php">войти</a> в аккаунт<br/> Логин : <strong>%s</strong>
        <br/> Пароль : <strong>%s</strong> ',
                strip_tags($_COOKIE['login']),
                strip_tags($_COOKIE['pass']));
        }
    }

    // Складываем признак ошибок в массив.
    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['sex'] = !empty($_COOKIE['sex_error']);
    $errors['bio'] = !empty($_COOKIE['bio_error']);
    $errors['check'] = !empty($_COOKIE['check_error']);
    $errors['abil'] = !empty($_COOKIE['abil_error']);
    $errors['year'] = !empty($_COOKIE['year_error']);
    $errors['limb'] = !empty($_COOKIE['limb_error']);
    // TODO: аналогично все поля.

    // Выдаем сообщения об ошибках.
    if ($errors['fio']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('fio_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Заполните имя корректно </div>';
    }
    if ($errors['email']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('email_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Заполните почту </div>';
    }
    if ($errors['sex']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('sex_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Выберите пол </div>';
    }
    if ($errors['bio']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('bio_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Введите биографию </div>';
    }
    if ($errors['check']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('check_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Подтвердите согласие </div>';
    }
    if ($errors['abil']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('abil_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Выберите сверхспособность </div>';
    }
    if ($errors['year']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('year_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Корректно введите дату рождения   </div>';
    }
    if ($errors['limb']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('limb_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Выберите количество конечностей   </div>';
    }


    // TODO: тут выдать сообщения об ошибках в других полях.
    $user = 'u26378';
    $pass = '3453674';
    $db = new PDO('mysql:host=localhost;dbname=u26378', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    // Складываем предыдущие значения полей в массив, если есть.
    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
    $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
    $values['sex_value'] = empty($_COOKIE['sex_value']) ? '' : strip_tags($_COOKIE['sex_value']);
    $values['bio_value'] = empty($_COOKIE['bio_value']) ? '' : strip_tags($_COOKIE['bio_value']);
    $values['check_value'] = empty($_COOKIE['check_value']) ? '' : strip_tags($_COOKIE['check_value']);
    $values['abil_value'] = empty($_COOKIE['abil_value']) ? '' : strip_tags($_COOKIE['abil_value']);
    $values['year_value'] = empty($_COOKIE['year_value']) ? '' : strip_tags($_COOKIE['year_value']);
    $values['limb_value'] = empty($_COOKIE['limb_value']) ? '' : strip_tags($_COOKIE['limb_value']);
    // TODO: аналогично все поля.
    $flag = 0;
    foreach($errors as $err){ //Проверка ошибок
        if($err==1)$flag=1;
    }

    if (!$flag&&!empty($_COOKIE[session_name()]) &&
        session_start() && !empty($_SESSION['login'])) {
            $login = $db->quote($_SESSION['login']);
            $stmt = $db->prepare("SELECT id FROM users WHERE login = $login");
            $stmt->execute();
            $user_id='';
            while($row = $stmt->fetch())
            {
                $user_id=$row['id'];
            }

            $request = "SELECT name,email,year,sex,limb,bio,checkbox FROM form WHERE id = $user_id";
            $result = $db -> prepare($request);
            $result ->execute();

            $data = $result->fetch(PDO::FETCH_ASSOC);

        $values['fio'] = strip_tags($data['name']);
        $values['email'] = strip_tags($data['email']);
        $values['year_value'] = strip_tags($data['year']);
        $values['sex_value'] = strip_tags($data['sex']);
        $values['limb_value'] = strip_tags($data['limb']);
        $values['bio_value'] = strip_tags($data['bio']);
        $values['check_value'] = strip_tags($data['checkbox']);



        $login_ses=strip_tags($_SESSION['login']);
        $uid_ses=strip_tags($_SESSION['uid']);

        echo '<head>
    <script src="https://kit.fontawesome.com/e2ac9cc532.js" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link  href="style-form.css" rel="stylesheet"  media="all"/>
</head>
<div class="formname">
<label style="margin-top:50px;text-align: center">Вход выполнен : <br/><strong>Логин </strong>: '.$login_ses.' <br/><strong>Ваш id </strong>:  '.$uid_ses.' </label>
</div>';
            print '

    <head>
        <script src="https://kit.fontawesome.com/e2ac9cc532.js" crossorigin="anonymous"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link  href="style-form.css" rel="stylesheet"  media="all"/>
    </head>
            <form action="login.php" accept-charset="UTF-8" method="GET">
                    <input  style="color:white;" type="submit" id="send" class="buttonform" value="Выйти">
                    
        </form>
';
    }else{
       print '

<head>
        <script src="https://kit.fontawesome.com/e2ac9cc532.js" crossorigin="anonymous"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link  href="style-form.css" rel="stylesheet"  media="all"/>
    </head>
            <form action="login.php" accept-charset="UTF-8" method="GET">
                    <input  style="margin-bottom:-100px;color:white;" type="submit" id="send" class="buttonform" value="Авторизация">
        </form>
        ';
    }

    // Включаем содержимое файла form.php.
    // В нем будут доступны переменные $messages, $errors и $values для вывода
    // сообщений, полей с ранее заполненными данными и признаками ошибок.
    include('form.php');
} else {
    if (!isset($_COOKIE['admin'])) {
        setcookie('admin', '0');
    }

    // Проверяем ошибки.
    $errors = FALSE;
    if (empty(strip_tags($_POST['fio'])) || (preg_match("/^[a-z0-9_-]{2,20}$/i", strip_tags($_POST['fio'])))) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('fio_value', strip_tags($_POST['fio']), time() + 365 * 24 * 60 * 60);
    }
    if (empty(strip_tags($_POST['email']))) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('email_value', strip_tags($_POST['email']), time() + 365 * 24 * 60 * 60);
    }
    if (!isset($_POST['radio2'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('sex_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('sex_value', strip_tags($_POST['radio2']), time() + 365 * 24 * 60 * 60);
    }
    if (empty(strip_tags($_POST['textarea1']))) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('bio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('bio_value', strip_tags($_POST['textarea1']), time() + 365 * 24 * 60 * 60);
    }
    if (!isset($_POST['checkbox'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('check_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('check_value', strip_tags($_POST['checkbox']), time() + 365 * 24 * 60 * 60);
    }
    $kek = 0;
    $myselect = $_POST['select1'];
    for ($i = 0; $i < 5; $i++) {
        if ($myselect[$i] != NULL) {
            $kek = 1;
        }
    }
    if (!$kek) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('abil_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('abil_value', strip_tags($_POST['select1']), time() + 365 * 24 * 60 * 60);
    }
    if (!isset($_POST['radio1'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('limb_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('limb_value', strip_tags($_POST['radio1']), time() + 365 * 24 * 60 * 60);
    }
    if (empty(strip_tags($_POST['birthyear'])) || strip_tags($_POST['birthyear']) < 1886 || strip_tags($_POST['birthyear']) > 2021) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('year_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('year_value', strip_tags($_POST['birthyear']), time() + 365 * 24 * 60 * 60);
    }


    if ($errors) {
        // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
        header('Location: index.php');
        exit();
    } else {
        $user = 'u26378';
        $pass = '3453674';
        $db = new PDO('mysql:host=localhost;dbname=u26378', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
        //Удаляем Cookies с признаками ошибок.
        setcookie('fio_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('sex_error', '', 100000);
        setcookie('bio_error', '', 100000);
        setcookie('check_error', '', 100000);
        setcookie('abil_error', '', 100000);
        setcookie('year_error', '', 100000);
        setcookie('limb_error', '', 100000);
        //удалить остальные Cookies.
        //
        if (!empty($_COOKIE[session_name()]) &&
            session_start() && !empty($_SESSION['login'])&&!empty($_SESSION['uid'])) {

            $fio = $db->quote($_POST['fio']);
            $email = $db->quote($_POST['email']);
            $birthyear = $db->quote($_POST['birthyear']);
            $sex = $db->quote($_POST['radio2']);
            $limb = $db->quote($_POST['radio1']);
            $bio = $db->quote($_POST['textarea1']);

            $login = strip_tags($_SESSION['login']);
            $stmt = $db->prepare("SELECT id FROM users WHERE login = '$login'");
            $stmt->execute();
            $user_id='';
            while($row = $stmt->fetch())
            {
                $user_id=$row['id'];
            }

            $sql = "UPDATE form SET name='$fio',email='$email',year='$birthyear',sex='$sex',limb='$limb',bio='$bio' WHERE id='$user_id'";
            $stmt = $db->prepare($sql);
            $stmt->execute();



           
            $ability_data = ['god','wall','levity','kos','kol'];
            $abilities = strip_tags($_POST['select1']);
            $ability_insert = [];
            foreach ($ability_data as $ability) {
                $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;

            }
            $god=$ability_insert['god'];
            $wall=$ability_insert['wall'];
            $levity=$ability_insert['levity'];
            $kos=$ability_insert['kos'];
            $kol=$ability_insert['kol'];
            $sql = "UPDATE all_abilities SET ability_god='$god',ability_through_walls='$wall',ability_levity='$levity',ability_kostenko='$kos',ability_kolotiy='$kol' WHERE id='$user_id'";
            $stmt = $db->prepare($sql);
            $stmt->execute();

        }
        else {
            // Генерируем уникальный логин и пароль.
            // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
            $login=generate(rand(1,25));

            $pass =generate(rand(1,25));

            $hash_pass=password_hash($pass, PASSWORD_DEFAULT);
            // Сохраняем в Cookies.
            setcookie('login', $login);
            setcookie('pass', $pass);

            // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.
            // complete


            $user = 'u26378';
            $pass = '3453674';
            $db = new PDO('mysql:host=localhost;dbname=u26378', $user, $pass, array(PDO::ATTR_PERSISTENT => true));


            $stmt = $db->prepare("INSERT INTO form (name, year,email,sex,limb,bio,checkbox) VALUES (:fio, :birthyear,:email,:sex,:limb,:bio,:checkbox)");
            $stmt->execute(array('fio' => strip_tags($_POST['fio']), 'birthyear' => strip_tags($_POST['birthyear']), 'email' => strip_tags($_POST['email']), 'sex' => strip_tags($_POST['radio2']), 'limb' => strip_tags($_POST['radio1']), 'bio' => strip_tags($_POST['textarea1']), 'checkbox' => strip_tags($_POST['checkbox'])));


            $ability_data = ['god','wall','levity','kos','kol'];
            $abilities = strip_tags($_POST['select1']);
            $ability_insert = [];
            foreach ($ability_data as $ability) {
                $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;

            }

            $stmt = $db->prepare("INSERT INTO all_abilities (ability_god, ability_through_walls,ability_levity,ability_kostenko,ability_kolotiy) VALUES (:god, :wal,:lev,:kos,:kol)");

            //$stmt->execute(array('god'=>$ability_insert['god'],'wall'=>$ability_insert['wall'],'levity'=>$ability_insert['levity'],'kostenko'=>$ability_insert['kos'],'kolotiy'=>$ability_insert['kol']));
            $stmt->bindParam(':god', $ability_insert['god']);
            $stmt->bindParam(':wal', $ability_insert['wall']);
            $stmt->bindParam(':lev', $ability_insert['levity']);
            $stmt->bindParam(':kos', $ability_insert['kos']);
            $stmt->bindParam(':kol', $ability_insert['kol']);
            $stmt->execute();

            $stmt = $db->prepare("INSERT INTO users (login, hash) VALUES (:login,:hash)");
            $stmt->bindParam(':login', $login);
            $stmt->bindParam(':hash', $hash_pass);
            $stmt->execute();


        }
        setcookie('save', '1');
        header('Location: index.php');
    }
}


